import re

with open('re_teksty.txt', encoding='utf-8') as f:
    tresc = f.read() # wczytaj cały plik jako jeden string

kody = re.findall(r'\d{2}-\d{3}', tresc)
print('Kody pocztowe: ', kody)

# Gdyby nie było ?: , to findall zwróciłby wartości tej grupy, czyli pierwszy człon nazwy domeny z emaila.
# Jeśli tę grupę oznaczymy jako anonimową, to findall zwraca całe napisy.
emaile = re.findall(r'[\w\-]+@(?:\w+\.)+\w+', tresc)
print('Adresy email: ', emaile)

daty = re.findall(r'\d{1,2} \w{3} \d{4}', tresc)
print('Daty: ', daty)
