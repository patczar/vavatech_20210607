def fib(count=1000):
    (a, b) = (0, 1)
    while count > 0:
        yield a
        (a, b) = (a + b, a)
        count -= 1


for x in fib(1000):
    print(x)

