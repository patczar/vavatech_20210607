import requests

ADRES='http://api.nbp.pl/api/exchangerates/tables/A/?format=json'

try:
    print('Pobieram dane...')
    dane = requests.get(ADRES).json()
    tabela = dane[0]
    print('Tabela numer', tabela['no'], 'z dnia', tabela['effectiveDate'])

    waluty = tabela['rates']

    kod = input('Podaj kod waluty, np. EUR: ').upper()
    for waluta in waluty:
        if waluta['code'] == kod:
            print(waluta['code'], waluta['currency'], waluta['mid'])
            kwota = float(input('Podaj kwotę do przeliczenia: '))
            wynik_pln = kwota * waluta['mid']
            print(f'{kwota:.2f} {kod} = {wynik_pln:.2f} PLN')
            wynik_waluta = kwota / waluta['mid']
            print(f'{kwota:.2f} PLN = {wynik_waluta:.2f} {kod}')

except Exception as e:
    print('Nie udało się pobrać', e)

print('Do widzenia')
