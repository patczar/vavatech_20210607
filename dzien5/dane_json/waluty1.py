# Z adresu http://api.nbp.pl/api/exchangerates/tables/a/?format=json
# W większości przypadków (ale nie w ogólności *...)
# dane w formacie JSON można po prostu wkleić do kodu Pythona i one zostaną poprawnie zinterpretowane

#* W JSON występują wartości false, true i null, a w Pythonie trzeba by pisać False, True, None

dane = [{"table": "A", "no": "110/A/NBP/2021", "effectiveDate": "2021-06-10",
         "rates": [{"currency": "bat (Tajlandia)", "code": "THB", "mid": 0.1182},
                   {"currency": "dolar amerykański", "code": "USD", "mid": 3.6821},
                   {"currency": "dolar australijski", "code": "AUD", "mid": 2.8500},
                   {"currency": "dolar Hongkongu", "code": "HKD", "mid": 0.4745},
                   {"currency": "dolar kanadyjski", "code": "CAD", "mid": 3.0399},
                   {"currency": "dolar nowozelandzki", "code": "NZD", "mid": 2.6453},
                   {"currency": "dolar singapurski", "code": "SGD", "mid": 2.7788},
                   {"currency": "euro", "code": "EUR", "mid": 4.4800},
                   {"currency": "forint (Węgry)", "code": "HUF", "mid": 0.012936},
                   {"currency": "frank szwajcarski", "code": "CHF", "mid": 4.1084},
                   {"currency": "funt szterling", "code": "GBP", "mid": 5.1876},
                   {"currency": "hrywna (Ukraina)", "code": "UAH", "mid": 0.1359},
                   {"currency": "jen (Japonia)", "code": "JPY", "mid": 0.03363},
                   {"currency": "korona czeska", "code": "CZK", "mid": 0.1761},
                   {"currency": "korona duńska", "code": "DKK", "mid": 0.6025},
                   {"currency": "korona islandzka", "code": "ISK", "mid": 0.030373},
                   {"currency": "korona norweska", "code": "NOK", "mid": 0.4421},
                   {"currency": "korona szwedzka", "code": "SEK", "mid": 0.4447},
                   {"currency": "kuna (Chorwacja)", "code": "HRK", "mid": 0.5976},
                   {"currency": "lej rumuński", "code": "RON", "mid": 0.9101},
                   {"currency": "lew (Bułgaria)", "code": "BGN", "mid": 2.2906},
                   {"currency": "lira turecka", "code": "TRY", "mid": 0.4309},
                   {"currency": "nowy izraelski szekel", "code": "ILS", "mid": 1.1346},
                   {"currency": "peso chilijskie", "code": "CLP", "mid": 0.005115},
                   {"currency": "peso filipińskie", "code": "PHP", "mid": 0.0771},
                   {"currency": "peso meksykańskie", "code": "MXN", "mid": 0.1867},
                   {"currency": "rand (Republika Południowej Afryki)", "code": "ZAR", "mid": 0.2695},
                   {"currency": "real (Brazylia)", "code": "BRL", "mid": 0.7272},
                   {"currency": "ringgit (Malezja)", "code": "MYR", "mid": 0.8940},
                   {"currency": "rubel rosyjski", "code": "RUB", "mid": 0.0510},
                   {"currency": "rupia indonezyjska", "code": "IDR", "mid": 0.00025844},
                   {"currency": "rupia indyjska", "code": "INR", "mid": 0.050374},
                   {"currency": "won południowokoreański", "code": "KRW", "mid": 0.003301},
                   {"currency": "yuan renminbi (Chiny)", "code": "CNY", "mid": 0.5761},
                   {"currency": "SDR (MFW)", "code": "XDR", "mid": 5.3042}]}]

#print(dane)
print('dane są typu:', type(dane))

tabela = dane[0]
print('tabela jest typu:', type(tabela))
print('Tabela nr', tabela["no"], 'z dnia', tabela["effectiveDate"])

rates = tabela["rates"]
print('waluty jest typu:', type(rates))

for rate in rates:
    # print('Pojedyncza waluta jest typu', type(waluta))
    print(f'{rate["currency"]:36} ({rate["code"]}) → {rate["mid"]:12.9f}')
