# W tej wersji używamy wyspecjalizowanej biblioteki requests, którą doinstalować poleceniem pip (albo w konfiguracji projektu w PyCharm)

import requests

ADRES='http://api.nbp.pl/api/exchangerates/tables/A/?format=json'

print('Pobieram dane...')
dane = requests.get(ADRES).json()

print('dane są typu:', type(dane))
tabela = dane[0]
print('tabela jest typu:', type(tabela))
print('Tabela nr', tabela["no"], 'z dnia', tabela["effectiveDate"])

rates = tabela["rates"]
print('waluty jest typu:', type(rates))
print()

for rate in rates:
    # print('Pojedyncza waluta jest typu', type(waluta))
    print(f'{rate["currency"]:36} ({rate["code"]}) → {rate["mid"]:12.9f}')


# Zadanie 1:
# Program pyta o datę w formacie YYYY-MM-DD i próbuje pobrać kursy walutowe z podanego dnia
# Jeśli user poda pustego stringa, to pobieranie są kursy bieżące.

# Zadanie 2:
# Program ładuje kursy walutowe i następnie prosi usera o podanie kodu waluty (np. EUR).
# Po podaniu prawidłowego kodu wypisuje kurs tej waluty i prosi o podaniekwoty do przeliczenia
# Następnie przelicza podaną kwotę wg kursu tej waluty.
# Sprytne podejście: raz pytamy o kwotę, a potem od razu przeliczamy w obie strony
# Np. 500 EUR to 2300 PLN, 500 PLN to 120 EUR

