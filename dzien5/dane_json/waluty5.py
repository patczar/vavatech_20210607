import requests

ADRES='http://api.nbp.pl/api/exchangerates/tables/A/?format=json'
try:
    print('Pobieram dane...')
    dane = requests.get(ADRES).json()
    tabela = dane[0]
    print('Tabela numer', tabela['no'], 'z dnia', tabela['effectiveDate'])
    waluty = tabela['rates']

    for waluta in waluty:
        # print('Pojedyncza waluta jest typu', type(waluta))
        print(f'{waluta["currency"]:36} ({waluta["code"]}) → {waluta["mid"]:12.9f}')

    print()
    while True:
        szukana_waluta = input('Podaj kod waluty: ').upper()
        if not szukana_waluta: break

        for waluta in waluty:
            if waluta['code'] == szukana_waluta:
                kod = waluta['code']
                nazwa = waluta['currency']
                kurs = waluta['mid']
                print(f'Waluta {nazwa} (kod {kod}) ma kurs {kurs}')
                break
        else:
            print(f'Nie ma takiej waluty {szukana_waluta}')

except Exception as e:
    print('Nie udało się pobrać', e)
