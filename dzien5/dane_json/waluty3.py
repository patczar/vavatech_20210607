# Używamy dwóch modułów ze standardowej biblioteki Pythona
import json
from urllib import request

ADRES='http://api.nbp.pl/api/exchangerates/tables/A/?format=json'

print('Pobieram dane...')
with request.urlopen(ADRES) as response:
    txt = response.read().decode('utf-8')

dane = json.loads(txt)
print('dane są typu:', type(dane))

tabela = dane[0]
print('tabela jest typu:', type(tabela))
print('Tabela nr', tabela["no"], 'z dnia', tabela["effectiveDate"])

rates = tabela["rates"]
print('waluty jest typu:', type(rates))

for rate in rates:
    # print('Pojedyncza waluta jest typu', type(waluta))
    print(f'{rate["currency"]:36} ({rate["code"]}) → {rate["mid"]:12.9f}')


