import requests

print('Aby pobrać bieżące kursy, podaj pusty napis')
print('Aby pobrać kursy z danego dnia, podaj datę w formacie yyyy-mm-dd')
print('Aby zakończyć, wpisz K')

while True:
    data = input('> ')
    if data.upper() == 'K': break

    ADRES=f'http://api.nbp.pl/api/exchangerates/tables/A/{data}?format=json'

    try:
        print('Pobieram dane...')
        dane = requests.get(ADRES).json()
        tabela = dane[0]
        print('Tabela numer', tabela['no'], 'z dnia', tabela['effectiveDate'])

        waluty = tabela['rates']
        for waluta in waluty:
            print(waluta['code'], waluta['currency'], waluta['mid'])

    except Exception as e:
        print('Nie udało się pobrać', e)
    print()

print('Do widzenia')
