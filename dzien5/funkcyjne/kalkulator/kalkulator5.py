x = input('Podaj pierwszą liczbę: ')
y = input('Podaj drugą liczbę: ')
op = input('Podaj rodzaj operacji: ')

# Uwaga: eval jest niebezpieczny, bo wykonuje (prawie) dowolny kod
wynik = eval(f'{x} {op} {y}')

print('Wynik', wynik)
