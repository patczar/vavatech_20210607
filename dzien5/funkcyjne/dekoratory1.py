print('Początek pliku')

# Dekorator to jest "funkcja na funkcjach".
# Jego celem jest zmodyfikowanie funkcji, którą definiuje programista.
# Ten dekorator wypisuje dodatkowe printy oraz zmienia wynik funkcji na pisany wielkimi literami (o ile jest to napis).
def dekorator(funkcja):
    print('dekorator: zostałem uruchomiony')
    def zmieniona_funkcja():
        print('zmieniona funkcja: początek')
        wynik = funkcja()
        if type(wynik) == str:
            wynik = wynik.upper()
        print('zmieniona funkcja: koniec')
        return wynik

    return zmieniona_funkcja


print('Zaraz zdefiniuję funkcję aaa')

@dekorator
def aaa():
    print('aaa, kotki dwa')
    return 'Ala ma kota'

print('Funkcja aaa zdefiniowana')
print()

print('Zaraz uruchomię funkcję aaa')
w = aaa()
print('Wynik w programie:', w)
print()

# Dodanie dekoratora przed funkcją aaa() działa tak, jak poniżej przekazanie funkcji bbb() do dekoratora jako parametru
def bbb():
    print('Basia ma rybkę')
    return 'rybka'

zmienione_bbb = dekorator(bbb)
w = zmienione_bbb()
print('Wynikiem ostatecznym jest:', w)
print()


@dekorator
def ccc():
    print('ccc: Celina ma chomika')
    return 'chomik'

ccc()
ccc()


