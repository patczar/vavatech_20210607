# Uwaga, to (jeszcze?) nie działa

def dopisywacz(poczatek='Początek', koniec='Koniec'):
    def nowy_dekorator(funkcja):
        def nowa_funkcja(*args, **kwargs):
            print(poczatek)
            try:
                return funkcja(*args, **kwargs)
            finally:
                print(koniec)

        return nowa_funkcja

    return nowy_dekorator


@dopisywacz
def aaa():
    print('Ala ma kota')

@dopisywacz('BBB start', 'BBB end')
def bbb():
    print('Ola ma psa')


aaa()
bbb()



