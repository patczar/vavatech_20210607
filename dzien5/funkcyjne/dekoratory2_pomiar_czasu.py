zmierzony_czas = 0.0

def pomiar_czasu(funkcja):
    from datetime import datetime
    def nowa_funkcja(*args, **kwargs):
        global zmierzony_czas
        poczatek = datetime.now()
        try:
            wynik = funkcja(*args, **kwargs)
            return wynik
        finally:
            koniec = datetime.now()
            zmierzony_czas = koniec - poczatek

    return nowa_funkcja


@pomiar_czasu
def fibr(n):
    if n == 0: return 0
    if n == 1: return 1
    return fibr(n-2) + fibr(n-1)


@pomiar_czasu
def fib(n):
    a, b = 0, 1
    for i in range(n):
        a, b = a+b, a
    return a


@pomiar_czasu
def czy_pierwsza(liczba):
    if liczba <= 1:
        return False
    for i in range(2, liczba):
        if liczba % i == 0:
            return False
    return True


def main():
    while True:
        n = int(input('Podaj parametr: '))
        if n < 0: break
        wynik = fibr(n)
        print(f'F({n}) = {wynik}')
        print('Obliczenie fib trwało:', zmierzony_czas)
        if czy_pierwsza(wynik):
            print(f'Liczba {wynik} JEST liczbą pierwszą')
        else:
            print(f'Liczba {wynik} NIE JEST liczbą pierwszą')
        print('Obliczenie pierwszej trwało:', zmierzony_czas)
        print()


if __name__ == '__main__':
    main()

