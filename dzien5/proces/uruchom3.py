import subprocess

KATALOG_PROJEKTU = '/home/patryk/PycharmProjects/vavatech_20210607/'

print('Zaraz zacznę')

result = subprocess.run(['find', KATALOG_PROJEKTU, '-name', '*.py'],
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
output = result.stdout
tekst = output.decode()
linie = tekst.splitlines(keepends=False)

for linia in linie:
    print('*', linia)

print('Gotowe')
