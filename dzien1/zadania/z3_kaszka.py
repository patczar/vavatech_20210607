'''
Napisz program, który pyta użytkownika o wiek, a następnie na podstawie wieku proponuje posiłek/napój ;-)

* 0-3 lat → kaszka z mleczkiem
* 4-17 lat → chipsy z colą
* 18-64 lat → piwo
* 65+ → biovital
'''

wiek = int(input('Ile masz lat? '))

# Kilka możliwych wersji zapisu

# Pierwsza wersa, trochę nadmiarowy zapis.
if wiek >= 0 and wiek <= 3:
    print('kaszka z mleczkiem')
elif wiek >= 4 and wiek <= 17:
    print('chipsy z colą')
elif wiek >= 18 and wiek <= 64:
    print('piwo')
elif wiek >= 65:
    print('Biovital')


# Druga wersja nie jest lepsza, tylko jest inna.
# Skoro warunki logiczne są samowystarczalne (w pełni opisują sytuację),
# to zamiast elif, możemy używać kolejnych instrukcji if
# Dzięki temu nie będzie lepiej, będzie nawet mniej wydajnie,
# bo Python sprawdzi zawsze wszystkie warunki, ale po prostu pokazuję, że tak też zadziała.

# Inna zmiana: Zamiast pisać w jednym miejscu 17, a w drugim 18 (łatwiej pogubić), będziemy pisać w obu miejscach 18 (program będzie bardziej czytelny)

if wiek >= 0 and wiek < 4:
    print('kaszka z mleczkiem')
if wiek >= 4 and wiek < 18:
    print('chipsy z colą')
if wiek >= 18 and wiek < 65:
    print('piwo')
if wiek >= 65:
    print('Biovital')
if wiek < 0:
    print('Ujemny wiek')


# Tutaj wykorzystujemy sprawdzanie przedziałów.
# Ponownie wracam do if/elif/else, bo to jest bardziej wydajne.

if wiek < 0:
    print('Ujemny wiek')
elif 0 <= wiek < 4:
    print('kaszka z mleczkiem')
elif 4 <= wiek < 18:
    print('chipsy z colą')
elif 18 <= wiek < 65:
    print('piwo')
elif 65 <= wiek:
    print('Biovital')


# Ta wersja jest wg mnie najlepsza.
# Jeśli podano ujemny wiek, to zostanie wypisane "Ujemny wiek" i program nie pójdzie dalej,
# nie będzie sprawdzał kolejnych warunków. Możemy więc w kolejnych warunkach nie sprawdzać tych rzeczy,
# które już zostały sprawdzone.
# Inaczej mówiąc w konstrukcji if/elif/else zostanie wykonana tylko jedna gałąź - pierwsza, dla której warunek był prawdziwy.

if wiek < 0:
    print('Ujemny wiek')
elif wiek < 4:
    print('kaszka z mleczkiem')
elif wiek < 18:
    print('chipsy z colą')
elif wiek < 65:
    print('piwo')
else:
    print('Biovital')
