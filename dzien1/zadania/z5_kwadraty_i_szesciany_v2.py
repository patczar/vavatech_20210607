# Za pomocą pętli for wypisz liczby od 1 do podanego zakresu (weź 1000, albo zapytaj użytkownika)
# W każdym wierszu ma być wypisania liczba a obok niej jej kwadrat (druga potęga) oraz sześcian (trzecia potęga)
# Dla chętnych: spróbujcie wyrównać kolumny

for x in range(1, 1001):
    print(f'| {x:4} | {x**2:7} | {x**3:10} |')
