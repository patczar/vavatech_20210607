# Napisz program obliczający koszt przejazdu z miasta A do B na
# podstawie podanej przez użytkownika liczby kilometrów, ceny paliwa
# oraz spalania. Zapytaj użytkownika także o nazwy miejscowości (pełnią rolę ozdobną, nie wpływają na wynik).

# Przykładowe komunikaty programu:
# Miasto A: Warszawa
# Miasto B: Katowice
# Dystans Warszawa-Katowice: 300
# Cena paliwa: 5.20
# Spalanie na 100 km: 6.0
# Koszt przejazdu Warszawa-Katowice to 78.00 zł

miasto1 = input("Miasto A: ")
miasto2 = input("Miasto B: ")
dystans = float(input(f"Dystans {miasto1}-{miasto2}: "))
cena = float(input("Cena paliwa: "))
spalanie = float(input("Spalanie na 100 km: "))

koszt = dystans * cena * spalanie / 100
# print(f"Koszt przejazdu {miasto1}-{miasto2} to {koszt} PLN")

# Chyba najlepiej wypisać z dwoma miejscami po kropce
print(f"Koszt przejazdu {miasto1}-{miasto2} to {koszt:.2f} PLN")
