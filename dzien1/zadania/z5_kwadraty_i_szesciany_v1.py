# Za pomocą pętli for wypisz liczby od 1 do podanego zakresu (weź 1000, albo zapytaj użytkownika)
# W każdym wierszu ma być wypisania liczba a obok niej jej kwadrat (druga potęga) oraz sześcian (trzecia potęga)

# pisane na szybko:
for x in range(1000):
    print(x, x**2, x**3)
