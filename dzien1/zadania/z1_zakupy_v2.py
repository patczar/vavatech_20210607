# Program pyta użytkownika o następujące dane:
# * nazwa towaru       np. "Co chcesz kupić?"
# * cenę jednostkową   np. "Ile kosztuje jedna sztuka?"
# * liczba sztuk       np. "Ile sztuk chcesz kupić?"

# Program oblicza ile będzie do zapłaty za tyle sztuk podanego towaru i wypisuje komunikat przypominający:
# Za 2 szt. towaru kawa zapłacisz 12.00 zł

nazwa = input("Jaki towar chcesz kupić? ")
cena = float(input("Ile kosztuje jedna sztuka? "))
ilosc = int(input("Ile sztuk chcesz kupić? "))

kwota = cena * ilosc
print(f"Za {ilosc} szt. towaru {nazwa} zapłacisz {kwota:.2f} zł")
print('Kwota bezpośrednio', kwota)
