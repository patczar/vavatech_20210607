'''
Przewoźnik lotniczy ma następujące wymagania co do bagażu podręcznego:
 * żaden z trzech wymiarów nie może przekroczyć 50 cm
 * objętość bagażu nie może przekroczyć 10 000 cm³

Napisz program, który wczytuje trzy wymiary w cm i informuje czy bagaż spełnia normę, czy nie.
'''


a = int(input('Podaj wysokość: '))
b = int(input('Podaj szerokość: '))
c = int(input('Podaj głębokość: '))
print()

objetosc = a*b*c

if a <= 50 and b <= 50 and c <= 50 and objetosc <= 10000:
    print('OK, bagaż spełnia normę')
else:
    print('Źle, bagaż nie spełnia normy')

# alternatywnie
if a > 50 or b > 50 or c > 50 or objetosc > 10000:
    print('Źle, bagaż nie spełnia normy')
else:
    print('OK, bagaż spełnia normę')
print()

# alternatywnie
if a > 50 or b > 50 or c > 50:
    print('Za długi bok')
elif objetosc > 10000:
    print('Za duża łączna objętość')
else:
    print('OK, bagaż spełnia normę')
print()


# Można rozpisać jeszcze bardziej, ale wtedy sprawdzajmy warunki negatywne (że coś jest źle)
if a > 50:
    print('Za duże a')
elif b > 50:
    print('Za duże b')
elif c > 50:
    print('Za duże c')
elif objetosc > 10000:
    print('Za duża łączna objętość')
else:
    print('OK, bagaż spełnia normę')
print()

# Hint: W takich sytuacjach brdziej opłaca się sprawdzać warunki negatywne (gdy coś jest źle, to odrzucam i koniec tematu)
# niż warunki pozytywne (jeśli jedna rzecz dobrze, to wchodzę i sprawdzam kolejną)
# To powoduje powstawanie wielu poziomów wcięć,  a else jest daleko od swojego if-a
# Taki zapis to masakra, tak nie róbcie!
if a <= 50:
    if b <= 50:
        if c <= 50:
            if objetosc <= 10000:
                print('Wszystko OK')
            else:
                print('Za duża objętość')
        else:
            print('Za duże c')
    else:
        print('Za duże b')
else:
    print('Za duże a')
print()



# Wersja z listą błędów - analogia do walidacji formularzy internetowych:
# Tutaj nawet gdy wystąpi pierwszy błąd, to sprawdzamy kolejne warunki
errors = []
if a > 50:
    errors.append('Za duże a')
if b > 50:
    errors.append('Za duże b')
if c > 50:
    errors.append('Za duże c')
if objetosc > 10000:
    errors.append('Za duża łączna objętość')

# A dopiero na końcu sprawdzamy czy wystąpiły jakieś błedy
if errors:
    print('Wystąpiły błędy:', errors)
else:
    print('OK, bagaż spełnia normę')
