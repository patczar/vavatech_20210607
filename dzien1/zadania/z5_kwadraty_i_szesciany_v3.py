# Za pomocą pętli for wypisz liczby od 1 do podanego zakresu (weź 1000, albo zapytaj użytkownika)
# W każdym wierszu ma być wypisania liczba a obok niej jej kwadrat (druga potęga) oraz sześcian (trzecia potęga)
# Dla chętnych: spróbujcie wyrównać kolumny

limit = int(input('Podaj limit: '))

dlugosc = len(str(limit))

for x in range(1, limit+1):
    print(f'| {x:{dlugosc}} | {x**2:{dlugosc*2}} | {x**3:{dlugosc*3}} |')
