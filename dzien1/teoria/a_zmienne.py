# W Pythonie zmienną tworzy się za pomocą instrukcji przypisania:
x = 15

# Następnie można ją odczytać po prostu używając nazwy tej zmiennej
print(x)
print(10*x)

# Na zmienną można wpisać inną wartość
x = 100
print(x)

# Można też zmienić wartość za pomocą instrukcji takich jak += (co dokładnie można, to zależy od typu):
x += 5
print(x)
print()

# W Pythonie zmienna nie ma typu. Na zmienną można zawsze wpisać dowolną wartość i to TA WARTOŚĆ jest jakiegoś typu.
y = 'Ala ma kota'
# Teraz w zmiennej y znajduje się wartość typu str
print(y, 'typu', type(y))

y = 100
# Teraz w zmiennej y znajduje się wartość typu int
print(y, 'typu', type(y))

y = 3.14
print(y, 'typu', type(y))

y = ['pomidor', 'papryka', 'cebula']
print(y, 'typu', type(y))
print()

# Podczas tworzenia zmiennych można dopisać informację o typie ("type hint"):
z: int = 1234
print(z)
print(z, 'typu', type(z))

# To pełni rolę dokumentacji dla człowieka oraz wskazówki dla edytorów (PyCharm bardzo dobrze te rzeczy rozumie)
z = 4321 # tutaj edytor nie krzyczy, jest OK
print(z)

z = 'Ola ma psa' # tutaj edytor pokazuje błąd niezgodności typów, ale interpreter Pythona to zaakceptuje i wykona
print(z, 'typu', type(z))

# z += 6 # ERR
print()

# Próba odczytania zmiennej, która nie istnieje, kończy się wyjątkiem NameError.
# To jest błąd czasu działania - nie jest wykrywany wcześniej (ew. edytor typu PyCharm podświetli nam to na czerwono)
# print(Y)

# Przy okazji: wszystkie wyjątki można "wyłapywać", więc jeśli istnieje ryzyko, że taki błąd nastąpi, można napisać tak:
try:
    print(nieistnieje)
    print('Nie było błędu')
except Exception as e:
    print('Wystąpił błąd', e)

print('Dalszy ciąg programu')
print()

# Zmienna z istneije
print('z:', z)

# Za pomocą operacji del można usunąć zmienną
del z
# Teraz zmienna z już nie istnieje
# print('z:', z)


# W Pythonie poprawne jest wpisane w treści programu wyrażenia
2 * 7
'Ala ma kota'

# To wyrażenie jest wyliczane, ale wynik nigdzie się nie pojawia
print('Koniec')
