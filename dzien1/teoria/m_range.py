# Jedną z możliwości pobierania elementów, jest ich generowanie za pomocą range.
# range generuje kolejne liczby całkowite z jakieś przedziału
# Jest kilka możliwości użycia:

# range(koniec) - wygeneruje liczby od 0 włącznie do koniec wyłączając
# w tym przypadku od 0 do 9
print("\n=========")
print("range(10):")
for i in range(10):
   print(i)

print("\n=========")
print("range(10, 20):")
# range(poczatek, koniec) - wygeneruje liczby od poczatek włącznie do koniec wyłączając
# w tym przypadku od 10 do 19
for liczba in range(10, 20):
   print(liczba)

print("\n=========")
print("range(10, 24, 3):")
# range(poczatek, koniec, krok) - wygeneruje liczby od początek włącznie do koniec wyłączając,
# generując kolejne z podanym krokiem
# w tym przypadku: 10, 13, 16, 19, 22,
for liczba in range(10, 24, 3):
   print(liczba)
print()

print("range(100, 50, -5):")
for liczba in range(100, 50, -5):
   print(liczba)
print()

# range to nie jest kolekcja wszystkich tych liczb, tylko to jest generator, który potrafi je wygenerować
maszynka = range(0, 1000_000_000)
print(maszynka)
# for i in maszynka:
#     print(i)

