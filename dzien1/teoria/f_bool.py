b = False
print(b, type(b))
b = True
print(b, type(b))
b = 2 + 3 > 4
print(b, type(b))
print()


a = 3         # przypisanie
print(a == 3) # porównanie
print(a != 3)
print(a < 4)
print(a <= 3)
print(a > 4)
print(a >= 3)
print(1 + 2 == 3)
print()

print("and")
print(True and True)  # True
print(True and False) # False
print(False and True) # False
print(False and False)# False

print("or")
print(True or True)   # True
print(True or False)  # True
print(False or True)  # True
print(False or False) # False

print("not")
print(not True) # False
print(not False) # True
print()

# Chociaż w Pythonie istnieją operatory & i |
# to one powinny być używane przede wszystkim do operacji na bitach (maski bitowe).
# Są również używane przy okazji przeciążania operatorów, np. przez bibliotekę pandas.
print(True & False)
print(True | False)
print()

liczba = 13
print(liczba % 2 != 0 and liczba > 10)
