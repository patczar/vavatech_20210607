# Różne sposoby wypisywania danych w Pythonie:
x = 123
y = 3
wynik = x * y

print('Hello world')
print(wynik)

# print pozwala wypisać kilka rzeczy na raz, podczas wypisywania rozdziela je spacją
print('Wynik wynosi', wynik)

print(x, 'razy', y, 'jest równe', x*y)
print()

print('Ala','Ola',       'Ela') # rozdziela pojedynczymi spacjami
# Aby odstępem nie była spacja, tylko coś innego, może podać parametr sep
print('Adam', 'Tomasz', 'Andrzej', sep=' oraz ') # rozdziela słowem oraz
print('Ala', 'Ola', 'Ela', 'Ula', sep=';')
print(12, 13, 14, 15, sep='')
print()

# Aby print nie przechodził do nowej linii, można ustawić pusty parametr end

print('Ala ma kota', end='')
print('Ola', 'ma', 'psa', sep='.', end='!')
print('Koniec')
print()

# Jeśli mam kilka napisów i chcę połączyć w jeden, mogę użyć +
napis1 = 'Jest godzina 12:30'
napis2 = ', więc za pół godziny obiad.'

wynik = napis1 + napis2
print(wynik)

print(napis1 + napis2)
print(napis1 + ' ' + napis2)

# Ale dodawanie za pomocą + nie zadziała gdy do tekstu chcemy dodać liczbę:
# Tak można robić w Javie i C#, ale nie w Pythonie.
ilosc_kotow = 5
# wynik = 'Ala ma ' + ilosc_kotow + ' kotów.'
# print('Ala ma ' + ilosc_kotow + ' kotów.')


# Sposoby na łączenie fragmentów tekstu z wartościami
# 0) rzeczy nie będące tekstami konwertujemy na tekst za pomocą str()
# To jest raczej styl Javy, a nie Pythona, więc raczej nie powtarzajcie
wynik = 'Ala ma ' + str(ilosc_kotow) + ' kotów.'
print(wynik)

# 1) Podejście klasyczne: operator % ("modulo"). Analogia do funkcji printf z języka C:
# Bardzo popularne w Pythonie 2 i ogólnie w starych programach Pythona
print('%d razy %d jest równe %d' % (x, y, x*y))
# d jest od decimal, a inne formaty to np. x - hexcadecimal, s - string, f - float

imie = 'Alicja'
pi = 3.14159
print('%s ma %f kotów, a %d szestastkowo zapisuje się jako %x' % (imie, pi, x, x))
print()

# 2) Funkcja format - coś pośredniego między 1) a 3), mi osobiście niezbyt się podoba
print('{0} razy {1} jest równe {2}'.format(x, y, x*y))
print('{} razy {} jest równe {}'.format(x, y, x*y))

# 3) f-string, dostępne od Pythona 3.6:
print(f'{x} razy {y} jest równe {wynik}')
print(f'{x} razy {y} jest równe {x*y}')
print(f'Zmienna: {x} Zwykły nawias: {{')
print()


# dodatkowa możliwości:
# wyrównywanie do określonej liczby pozycji
imie1 = 'Ala'
imie2 = 'Ewelina'
imie3 = 'Aleksandrowska'

print(imie1, 'ma kota')
print(imie2, 'ma kota')
print()
print('%10s ma kota' % imie1)
print('%10s ma kota' % imie2)
print('%-10s ma kota' % imie2)
print('%10s ma kota' % imie3)
print()

# W format i f-stringach napisy są wyróœnywane do lewej, a liczby do prawej.
# Można to zmienić za pomocą symboli > < =
print('{0:10} ma kota'.format(imie1))
print('{:10} ma kota'.format(imie2))
print('{:>10} ma kota'.format(imie2))
print('{:10} ma kota'.format(imie3))
print()
print(f'{imie1:10} ma kota')
print(f'{imie2:10} ma kota')
print(f'{imie2:<10} ma kota')
print(f'{imie3:10} ma kota')
print()

# zaokrąglanie do określonej liczby miejsc po przecinku
import math
p = math.sqrt(2)
print('pierwiastek:', p)

print('pierwiastek: %.3f' % p)
print('pierwiastek: {:.3f}'.format(p))
print(f'pierwiastek: {p:.3f}')
print()

# Każda z tych operacji (%, format, f-str) tak naprawdę tworzy napis
# Zamiast wypisywać go od razu na ekran, można też zapisać na zmiennej, zapisać w pliku, bazie danych itp...

txt = 'pierwiastek: %.3f' % p
print(type(txt), txt)

txt = 'pierwiastek: {:.3f}'.format(p)
print(type(txt), txt)

txt = f'pierwiastek: {p:.3f}'
print(type(txt), txt)
