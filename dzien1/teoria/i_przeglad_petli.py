# W Pythonie są 2 rodzaje pętli: while , for

# while WARUNEK:
#    TREŚĆ

x = 1
while x <= 10:
    print('x jeszcze się mieści, bo ma wartość', x)
    x += 1

print('Pętla się zakończyła, teraz x = ', x)
print()

y = 1000
while y > 1:
    print(y)
    y /= 2
print()

# Zasadniczo pętla for działa na zasadzie przeglądania kolekcji i wykonania czyności dla każdego elementu kolekcji.
# for zmienna in KOLEKCJA:
#    TREŚĆ

for imie in ['Ala', 'Ola', 'Ela', 'Ula']:
    print('Witaj', imie)
print()

# W Pythonie za pomocą pętli for w tak wygodny sposób można też przeglądać m.in.:
#  kolejnie linie pliku
#  kolejne rekordy z bazy danych

# Bardzo często używa się pętli for w połączeniu z range.
# range - generator liczb z określonego przedziału

for liczba in range(10, 20):
    print('Kolejna liczba to', liczba)

# W Pythonie nie ma takiej pętli for jak w C, C++, Java, JavaScript, C#, PHP
# for(int i = 0; i < n; i++)
