# Zamiast powtarzać jakąś instrukcję wiele razy
print('Ala ma kota')
print('   a Ola ma psa.')
print('Ala ma kota')
print('   a Ola ma psa.')
print('Ala ma kota')
print('   a Ola ma psa.')
print('Ala ma kota')
print('   a Ola ma psa.')
print('Ala ma kota')
print('   a Ola ma psa.')
print()

# powinniśmy użyć pętli:

for _ in range(5):
    print('Ala ma kota')
    print('   a Ola ma psa.')
print('Koniec pętli')
print()


# W Pythonie mamy dwa rodzaje pętli:
# Pętla while powtarza czynności dopóki prawdziwy jest warunek logiczny

print('początek pętli while')
x = 10
while x > 0:
    print('x ciągle jest większy niż 0')
    print('    bo jest równy', x)
    x -= 2
print('koniec pętli while')
print()

# Pętla for przegląda zawartość kolekcji, wpisując na zmienną poszczególne wartości
imiona = ['Ala', 'Ola', 'Ela']
for imie in imiona:
    print(f'Kolejna osoba ma na imię {imie}')
print('koniec pętli for')
print()

# Gdybyśmy chcieli przejrzeć zakres liczbowy, to najlepiej użyć for zmienna in range

for i in range(20, 30):
    print(i)
