liczby = [33, 21, 11, 27, 13]

for x in liczby:
    print('Oglądam liczbę', x)
    if x % 2 == 0:
        print('Znalazłem liczbę parzystą:', x)
        break # prejście do linii 12 z pominięciem else
    print('a kuku')
else:
    print('Nie znalazłem liczby parzystej') # wykona się jeśli nie było break

print('Jesteśmy poza pętlą for')
print()

# Pętle for oraz while mogą mieć dodatkowo dopisane else:
# Jeśli pętla kończy się z powodu break, to treść else nie jest wykonywana.
# Jeśli pętla kończy się "normalnie" (for doszedł do końca, while ma niespełniony warunek),
# to wtedy else się wykonuje. Również wtedy, gdy program w ogóle nie wszedł do pętli.
# Najczęstsze zastosowanie: jakaś forma wyszukiwania i wykonywania czynności w przypadku niezalezienia.

# Uwaga na while, b o to jest nieinstuicyjne.
# else wykona się także wtedy, gdy warunek na początku był prawdziwy, a while wykonał się X razy i w końcu warunek jest fałszywy.
a = 5
while a < 10:
    print('Jestem w while, a =', a)
    a += 2
else:
    print('else do while, teraz a =', a)
print('Jesteśmy poza pętlą while')


