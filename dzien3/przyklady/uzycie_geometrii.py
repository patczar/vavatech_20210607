# import geometria
# from geometria import pole_kwadratu, pole_prostokata, pole_kola
from geometria import *

print('\nPoczątek właściwego programu:')

a = float(input('Podaj długość boku a: '))
b = float(input('Podaj długość boku b: '))

pole_kw = pole_kwadratu(a)
print(f'Pole kwadratu o boku {a} wynosi: {pole_kw}')

pole_pr = pole_prostokata(a, b)
print(f'Pole prostokąta o bokach {a}×{b} wynosi: {pole_pr}')

pole_kl = pole_kola(a)
print(f'Pole koła o promieniu {a} wynosi: {pole_kl}')
