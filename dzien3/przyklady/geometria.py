# Zdefiniuj funkcje, które będą obliczać pole oraz obwód prostych figur geometrycznych...
# W tym jednym pliku zdefiniuj wszystkie te funkcje, a na końcu wpisz przykładowe wywołania i wypiosz ich wyniki
# Nie trzeba używać input

import math

def pole_kwadratu(a):
    return a*a

def obwod_kwadratu(a):
    return 4*a

def pole_prostokata(a, b):
    return a*b

def obwod_prostokata(a, b):
    return 2*a + 2*b

def pole_kola(r):
    return math.pi * r**2

def obwod_kola(r):
    return 2 * math.pi * r


print('pole kwadratu 5:', pole_kwadratu(5))
print('obwód kwadratu 5:', obwod_kwadratu(5))
print('pole prostokąta 3×4:', pole_prostokata(3, 4))
print('obwód prostokąta 3×4:', obwod_prostokata(3, 4))
print('pole koła 5:', pole_kola(5))
print('obwód koła 5:', obwod_kola(5))

