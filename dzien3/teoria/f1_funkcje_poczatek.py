# Zasadniczo program w języku Python to jest ciąg instrukcji pisanych jedna pod drugą.
print('Początek programu')

# W treści programu może znaleźć się definicja funkcji
# Gdy Python wczytuje instrukcję def, to zamapiętuje sobie treść funkcji pod podaną nazwą.
# Ale samej funkcji (instrukcji, które są w środku) w tym momencie nie wykonuje.
def aaa():
    print('Początek funkcji aaa')
    print('a a a, kotki dwa')
    print('Koniec funkcji aaa')

# To wypisze się jako druga linia outputu
print('Dalej jestem w zasadniczej treści programu')


# Aby treść się wykonała, należy funkcję "wywołać"
print('Teraz wywołam funkcję...')
aaa()
print('A teraz jestem z powrotem')
print()

# Zauważmy, że aaa można też traktować jak zmienną, np. tak:
print(aaa)

# Uruchomienie funkcji: aaa()        bbb(parametry)
# Odczytanie obiektu funkcja, np. po to, aby go przekazać, zapamiętać itp.:  aaa        bbb


print('Teraz znowu wywołam funkcję...')
aaa()
print()

# Można zdefiniwać funkcję, która w ogóle nie zostanie wykonana
def nieistotna():
    print('To się nigdy nie wypisze')
    print('Ani to')

print('Koniec programu')
