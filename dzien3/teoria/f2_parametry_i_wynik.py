# Funkcja może przyjmować parametry
def powitaj(imie, miasto):
    print('Witaj', imie, 'z miasta', miasto)


print('Początek programu')

powitaj('Ala', 'Gdańsk')
powitaj('Ola', 'Kraków')
# W Pythonie nie ma statycznej kontroli typów, więc do funkcji można przekazać dowolne wartości
powitaj({'Alicja', 'Aleksandra'}, 321)
print()

# W Pythonie parametry mogą mieć wartości domyślne (inaczej): niektóre parametry mogą być opcjonalne)
def powtorz(napis, ile_razy=1):
    for i in range(ile_razy):
        print(napis)
    print()

powtorz('Ola ma psa', 3)
powtorz('Ela ma chomika', 5)
powtorz('Ala ma kota') # 1 raz


# Funkcje mogą zwracać wynik
def ostatnia_cyfra(liczba):
    return liczba % 10

# Jeśli wywołam funkcję zwracającą wynik, to mogę ten wynik zapisać na zmienną
wynik = ostatnia_cyfra(113)
print('Ostatnia cyfra ze 113:', wynik)

# Można też użyć w dowolnym kontekście, gdzie oczekiwana jest wartość:
print(ostatnia_cyfra(1007)) # 7
print(500 + ostatnia_cyfra(44) + 1000)  # 1504

# Można też wywołać funkcję, która zwraca wynik, ale w ogóle z tym wynikiem nic nie robić
### !!! return to nie to samo co print !!! ###
ostatnia_cyfra(999) # 9 nigdzie się nie wypisze
print('Koniec z cyframi')
print()


# Najbardziej typowe funckcje matematyczne:

def do_kwadratu(x):
    return x ** 2

def pierwiastek(x):
    return x ** 0.5

print(do_kwadratu(5))
print(do_kwadratu(7))
print()
print(pierwiastek(25))
print(pierwiastek(2))
print()

# Gdy funkcja nie wykona instrukcji return, a dojdzie do końca, to jej wynikiem jest None
def byc_moze_cos_zwroce(x):
    if x % 2 == 0:
        return x // 2

print(byc_moze_cos_zwroce(16))
print(byc_moze_cos_zwroce(7))

