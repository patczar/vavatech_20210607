# z4 - ponumerowane linie Pana Tadeusza zapisz do nowego pliku


# v1 - najpierw czytam wszystkie linie, potem numeruję, potem zapisuję


with open('pan-tadeusz.txt', mode='r', encoding='utf-8') as wejscie:
    linie = wejscie.readlines()

nowe_linie = [f'{nr+1:5}. {linia}' for nr, linia in enumerate(linie)]

with open('ponumerowane1.txt', mode='w', encoding='utf-8') as wyjscie:
    wyjscie.writelines(nowe_linie)

print('Gotowe 1')
