# z5 - użytkownik podaje napis, np. Telimena
# a program wypisuje i zlicza te linie pliku, które zawierają podany napis

with open('pan-tadeusz.txt', mode='r', encoding='utf-8') as wejscie:
    linie = wejscie.readlines()

print(f'Wczytałem {len(linie)} linii z pliku')

while True:
    slowo = input('Jakiego słowa szukasz? ').strip().lower()
    if not slowo:
        break
    ile = 0
    for linia in linie:
        if slowo in linia.lower():
            print(linia.strip())
            ile += 1
    print(f'\nSłowo {slowo} występowało w {ile} liniach.')

print('pa pa')
