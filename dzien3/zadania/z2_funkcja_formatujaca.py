'''
Napisz funkcję formatuj, która jako parametry przyjmuje najpierw dowolnie wiele stringów podanych po przecinku,
a następnie dowolnie wiele parametrów przekazanych z nazwą (powiedzmy, że to są "zmienne").

Funkcja ma zwrócić w wyniku jeden napis, który będzie połączeniem wszystkich przekazanych stringów,
traktowanych jak kolejne linie, a w tych stringach wystąpienia "zmiennych" zapisane jako $x są zastępowane wartościami tych zmiennych.


wynik = formatuj('Ala ma $x kotów i $y psów.',
         'Towar $nazwa kosztuje $cena',
         'Może kupimy $nazwa?',
         x=5, y=6, nazwa='komputer', cena=1234.99, inny='pomidor')

To wynik powinien być napisem składającym się z trzech linii.
print(wynik)  powinno wypisać

Ala ma 5 kotów i 6 psów.
Towar komputer kosztuje 1234.99
Może kupimy komputer?

Podpowiedzi:
'\n'.join(lista)
napis.replace(co, naco)
'''

def formatuj(*linie, **zmienne):
    napis = '\n'.join(linie)
    for k, v in zmienne.items():
        napis = napis.replace('$'+k, str(v))
    return napis


wynik = formatuj('Ala ma $x kotów i $y psów.',
         'Towar $nazwa kosztuje $cena',
         'Może kupimy $nazwa?',
         x=5, y=6, nazwa='komputer', cena=1234.99, inny='pomidor')

print(wynik)
