# Zadanie 7: oblicz średnią pensję wszystkich pracowników

with open('pracownicy.csv', mode='r', encoding='utf-8') as plik:
    suma = 0
    ile = 0
    for linia in plik:
        t = linia.split(';')
        suma += float(t[4])
        ile += 1

srednia = suma / ile
print('Średnia:', srednia)

# Zadanie 8: oblicz średnią pensję programistów (stanowisko Programmer)
