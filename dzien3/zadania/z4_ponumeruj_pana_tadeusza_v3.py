# z4 - ponumerowane linie Pana Tadeusza zapisz do nowego pliku

# v3 - tutaj linie będą przepisywane z pliku do pliku na bieżąco. stosujemy podejście z iteratorem i otwieramy ob pliki w jednym with

with open('pan-tadeusz.txt', mode='r', encoding='utf-8') as wejscie,\
        open('ponumerowane3.txt', mode='w', encoding='utf-8') as wyjscie:
    for nr, linia in enumerate(wejscie):
        wyjscie.write(f'{nr:5}: {linia}')

print('Gotowe 3')
