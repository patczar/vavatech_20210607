# z5 - użytkownik podaje napis, np. Telimena
# a program wypisuje i zlicza te linie pliku, które zawierają podany napis

# W tej wersji użytrkownik podaje jedno słowo, a program dopiero wtedy jednorazowo czyta plik
# To jest wersja oszczędna pamięciowo.

slowo = input('Jakiego słowa szukasz? ')

ile = 0
with open('pan-tadeusz.txt', mode='r', encoding='utf-8') as wejscie:
    for linia in wejscie:
        if slowo in linia:
            print(linia.strip())
            ile += 1

print(f'\nSłowo {slowo} występowało w {ile} liniach.')
