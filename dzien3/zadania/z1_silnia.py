
# Zdefiniuj funkcję silnia, która dla podanej liczby całkowitej zwraca silnię tej liczby
# silnia to iloczyn kolejnych liczb naturalnych od 1 do n , np. 5! = 1*2*3*4*5 = 120

# I napisz program, który pyta użytkownika o liczbę, wywołuje funkcję silnia i wypisuje wynik
# Dla chętnych - niech to działa w pętli - niech użytkownik w jakiś sposób kończy program (do ustalenia)

# Opcjonalnie:
# Napisz funkcję, która dla podanej liczby całkowitej oblicza sumę jej cyfr
# Można wykorzystać w taki sposób, że obliczymy sumę cyfr tej silni, którą obliczyliśmy poprzednią funkcją.

def silnia(n):
    wynik = 1
    for i in range(2, n+1):
        wynik *= i
    return wynik


def suma_cyfr(liczba):
    suma = 0
    while liczba > 0:
        suma += liczba % 10
        liczba = liczba // 10  # ew. liczba //= 10
    return suma


print('Aby zakończyć, podaj liczbę ujemną.')
while True:
    liczba = int(input('Podaj liczbę: '))
    if liczba < 0:
        break
    wynik = silnia(liczba)
    print(f'silnia({liczba}) = {wynik}')
    print(f'suma cyfr silni: {suma_cyfr(wynik)}')
