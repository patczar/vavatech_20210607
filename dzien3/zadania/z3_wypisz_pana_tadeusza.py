# z3 Program czyta plik pan-tadeusz.txt i wypisuje na stdout całą jego treść
# numerując wszystkie linie, np. tak:
# 1. Adam Mickiewicz
# 2.
# 3. Pan Tadeusz czyli ostatni zajazd na Litwie

nr = 0
with open('pan-tadeusz.txt', mode='r', encoding='utf-8') as wejscie:
    for linia in wejscie:
        nr += 1
        # print(nr, linia, end='')
        print(f'{nr:5}. {linia.rstrip()}')
