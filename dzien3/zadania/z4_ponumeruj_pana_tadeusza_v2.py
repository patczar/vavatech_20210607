# z4 - ponumerowane linie Pana Tadeusza zapisz do nowego pliku

# v2 - tutaj linie będą przepisywane z pliku do pliku na bieżąco

with open('pan-tadeusz.txt', mode='r', encoding='utf-8') as wejscie:
    with open('ponumerowane2.txt', mode='w', encoding='utf-8') as wyjscie:
        nr = 0
        while True:
            nr += 1
            linia = wejscie.readline()
            if not linia: break
            print(nr, linia.strip(), file=wyjscie)

print('Gotowe 2')
