t = [20.0, 21.5, 18.7, 25.5, 31.0, 27.7, 22.33]

print(t)
print(len(t))
print()

# TODO Tak jak potrafisz :) oblicz średnią liczb z tej tablicy

# Ja zapiszę to na kilka sposobów
# Policzmy bezpośrednio na wartościach:
srednia1 = (20.0 + 21.5 + 18.7 + 25.5 + 31.0 + 27.7 + 22.2) / 7
print(srednia1)

# Zamiast kopiować wartości w kodzie, mogę pobierać je prosto z tablicy
srednia2 = (t[0] + t[1] + t[2] + t[3] + t[4] + t[5] + t[6]) / 7
print(srednia2)

# Wady podejścia nr 2: bezpośrednio wymieniam wszystkie pozycje w tablicy w jednym długim wyrażeniu.
# Gdyby elementów nie było 7 tylko 700, to trudno było to tak napisać

# Aby lepiej obliczyć sumę, przejdźmy na zapis ze "skarbonką" (książkowe określenie: akumulator),
# czyli zmienną, do której pojedynczo będziemy dodawać po jednej wartości.

suma = 0
suma += t[0]
suma += t[1]
suma += t[2]
suma += t[3]
suma += t[4]
suma += t[5]
suma += t[6]
# W tym momencie mam obliczoną sumę
srednia3 = suma / 7
print(srednia3)

# Tylko zamiast pisać 7 (albo 700) osobnych operacji, napiszmy jedną pętlę:
suma = 0
i = 0
while i < len(t):
    suma += t[i]
    i += 1
# W tym momencie mam obliczoną sumę
srednia4 = suma / len(t)
print(srednia4)

# To była pierwsza wersja poprawna, która zadziała także ta tablic innej wielkości
# Ale ta wersja, z pętlą while, nie jest napisana w stylu Pythona.

# Wersja z użyciem licznika, ale z wykorzystaniem generatora range
suma = 0
for i in range(len(t)):  # dla i od 0 do len-1
    suma += t[i]
srednia5 = suma / len(t)
print(srednia5)

# Wersja najbardziej w stylu Pythona - standardowy sposób uzycia pętli for
suma = 0
for x in t:
    suma += x
srednia6 = suma / len(t)
print(srednia6)

# Ilość elementów też można policzyć samodzielnie
suma = 0
licznik = 0
for x in t:
    suma += x
    licznik += 1
srednia7 = suma / licznik
print(srednia7)

# W Pythonie istnieją też funkcje wbudowane, więc najkrócej byłoby tak:
srednia9 = sum(t) / len(t)
print(srednia9)

# Jeśli chcesz zaokrąglić wynik np. do 3 miejsc po przecinku:
print(f'{srednia9:.2f}') # sposób od Python 3.6
print('%.2f' % srednia9) # sposób tradycyjny
