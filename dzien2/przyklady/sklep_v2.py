cennik = {
    'pralka': 2800,
    'odkurzacz': 690,
    'kuchenka': 3500,
}

print('Dostępne towary:')
for k, v in cennik.items():
    print(f'{k:12} {v:8.2f}')
print()

suma = 0
while True:
    towar = input('Podaj nazwę towaru (puste aby zakończyć): ')
    if not towar: break
    try:
        ilosc = int(input('Podaj ilość sztuk: '))
        wartosc = cennik[towar] * ilosc
        print(f'Za {ilosc} towaru {towar} zapłacicsz {wartosc:.2f} zł')
        suma += wartosc
    except ValueError:
        print('Niepoprawny format liczby')
    except KeyError:
        print('Nie ma takiego towaru')

print(f'W sumie do zapłaty {suma:.2f} zł')