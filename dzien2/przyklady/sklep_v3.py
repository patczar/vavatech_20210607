cennik = {
    'pralka': 2800,
    'odkurzacz': 690,
    'kuchenka': 3500,
}

while True:
    print('\nCo chcesz zrobić?')
    print('  Q - zakończ program')
    print('  P - wypisz cennik')
    print('  Z - zrób zakupy')
    print('  C - zmień cenę / dodaj towar')
    wybor = input('Wybierz: ').upper()

    if wybor == 'Q':
        break
    elif wybor == 'P':
        print('Dostępne towary:')
        for k, v in cennik.items():
            print(f'{k:12} {v:8.2f}')
    elif wybor == 'Z':
        suma = 0
        while True:
            towar = input('Podaj nazwę towaru (puste aby zakończyć): ')
            if not towar: break
            if not towar in cennik:
                print('Nie ma takiego towaru')
                continue
            try:
                ilosc = int(input('Podaj ilość sztuk: '))
                wartosc = cennik[towar] * ilosc
                print(f'Za {ilosc} towaru {towar} zapłacicsz {wartosc:.2f} zł')
                suma += wartosc
            except ValueError:
                print('Niepoprawny format liczby')
        print(f'W sumie do zapłaty {suma:.2f} zł')
    elif wybor == 'C':
        towar = input('Podaj nazwę towaru: ')
        if towar in cennik:
            print(f'Towar {towar} istnieje i ma cenę {cennik[towar]}')
        else:
            print('Nie ma takiego towaru, możesz go dodać')
        try:
            nowa_cena = float(input('Podaj nową cenę: '))
            cennik[towar] = nowa_cena
        except ValueError:
            print('Niepoprawny format liczby')
    else:
        print('Nieznana operacja')

print('Koniec programu')
