'''
Napisz program, który na początku odczytuje od użytkownika dowolnie dużo liczb typu float i dodaje je wszystkie do listy.
Jako sposób kończenia można przyjąć np. wpisanie pustego tekstu (naciśnięcie enter) bez liczby.

Następnie używając pętli (a nie gotowych wbudowanych funkcji) oblicz:
* ilość elementów
* sumę wartości
* średnią wartość
* minimalną oraz maksymalną wartość
'''

liczby = []

while True:
    txt = input('Podaj kolejną liczbę: ')
    if txt == '':  # ew. if not txt:
        break
    liczby.append(float(txt))

print('Zebrane liczby:', liczby)

# Drugi etap: wyliczamy statystyki

ilosc = 0
suma = 0
najwiekszy = float('-inf')
najmniejszy = float('+inf')

for liczba in liczby:
    ilosc += 1  # uwaga - w Pythonie nie ma operatora ++
    suma += liczba
    if liczba > najwiekszy:
        najwiekszy = liczba
    if liczba < najmniejszy:
        najmniejszy = liczba

print('Ilość:', ilosc)
print('Suma:', suma)
if ilosc > 0:
    srednia = suma / ilosc
    print('Średnia:', srednia)
    print('Największa:', najwiekszy)
    print('Najmniejsza:', najmniejszy)
