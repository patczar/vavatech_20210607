'''
Godzina parkowania kosztuje 3 zł.
Program na początku pyta o ilość godzina parkowania.
Użytkownik wprowadza liczbę, a program wylicza kwotę do zapłaty.

W pętli program prosi o wrzucenie kolejnej monety, a użytkownik wprowadza liczbę
tak długo, aż zostanie zapłacona cała należność.

Jeśli należy się reszta, to program wypisuje info ile reszty
'''

cena = 3
godziny = int(input('Ile godzin chcesz parkować: '))
do_zaplaty = cena * godziny
print(f'Do zapłaty {do_zaplaty} zł')

suma = 0
while suma < do_zaplaty:
    print(f'Do tej pory wpłacono {suma} zł. Do zapłaty pozostało {do_zaplaty - suma} zł')
    moneta = int(input('Wrzuć monetę: '))
    suma += moneta

if suma > do_zaplaty:
    print(f'Wydaję resztę {suma - do_zaplaty} zł')

print('Koniec')
