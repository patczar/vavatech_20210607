# Wypisz bez powtórzeń i oblicz ile jest liczb parzystych z zakresu od 0 do 99 wśród liczb podanych przez użytkownika.

liczby = set()
while True:
    s = input('Podaj liczbę: ')
    if not s: break
    try:
        liczby.add(int(s))
    except ValueError:
        print('To nie jest liczba')

print('Wprowadzone liczby:', liczby)
print('Jest ich', len(liczby))

wparzyste = set(range(0, 100, 2))
print('Wszystkie parzyste:', wparzyste)

parzyste = liczby & wparzyste
print('Parzyste podane przez użytkownika:', parzyste)
print('Liczba parzystych z zakresu:', len(parzyste))
