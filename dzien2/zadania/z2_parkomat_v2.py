'''
Godzina parkowania kosztuje 3 zł.
Program na początku pyta o ilość godzina parkowania.
Użytkownik wprowadza liczbę, a program wylicza kwotę do zapłaty.

W pętli program prosi o wrzucenie kolejnej monety, a użytkownik wprowadza liczbę
tak długo, aż zostanie zapłacona cała należność.

Jeśli należy się reszta, to program wypisuje info ile reszty
'''

cena = 3
godziny = int(input('Ile godzin chcesz parkować: '))
do_zaplaty = cena * godziny
print(f'Do zapłaty {do_zaplaty} zł')

while do_zaplaty > 0:
    print(f'Do zapłaty pozostało {do_zaplaty} zł')
    do_zaplaty -= int(input('Wrzuć monetę: '))

if do_zaplaty < 0:
    print(f'Wydaję resztę {-do_zaplaty} zł')

print('Koniec')
