wysokosc = int(input('Podaj liczbę wierszy: '))
szerokosc = int(input('Podaj liczbę kolumn:  '))

szer_kolumny = len(str(wysokosc * szerokosc))

for x in range(1, wysokosc+1):
    for y in range(1, szerokosc+1):
        print(f'{x*y:{szer_kolumny}} ', end='')
    print()
