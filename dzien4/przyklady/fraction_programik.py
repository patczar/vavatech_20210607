# z modułu fraction zaimportuj klasę Fraction
from fraction import Fraction

a = Fraction(3, 4)
b = Fraction(2, 3)
c = Fraction(1, 3)

print(a)
print(b)
print(c)
print()

print(b + c)
print(a + c)
print(a - b)
print(a * b)

print(a > b)

assert a > b
print('pierwsza asercja zadziałała')

assert a < b
print('druga asercja zadziałała')
