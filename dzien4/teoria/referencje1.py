class Konto:
    def __init__(self, numer, wlasciciel, saldo=0):
        self._numer = numer
        self._wlasciciel = wlasciciel
        self._saldo = saldo

    def __str__(self):
        return f'Konto nr {self._numer}, wł. {self._wlasciciel}, saldo: {self._saldo} PLN'

    def wplata(self, kwota):
        self._saldo += kwota

    def wyplata(self, kwota):
        self._saldo -= kwota



a = Konto(1, 'Ala', 1000)
b = Konto(2, 'Ola', 2000)
c = b

print('a:', a)
print('b:', b)
print('c:', c)
print()

# zmiana stanu obiektu, zmiana wewnątrz obiektu - zmienna c też widzi zmianę
b.wplata(40)
print('a:', a)
print('b:', b)
print('c:', c)
print()

c.wplata(8)
print('a:', a)
print('b:', b)
print('c:', c)
print()

# zmiana wewnątrz obiektu - zmienna c też widzi zmianę
# b._wlasciciel = 'Aleksandra'
# print('a:', a)
# print('b:', b)
# print('c:', c)
# print()

# zmiana w samej zmiennej b. Wpisanie do b "wskaźnika" do innego obiektu
# to nie wpływa na inne zmienne, zmiana NIE JEST widoczna poprzez c
b = a
print('a:', a)
print('b:', b)
print('c:', c)
print()

c = a
# teraz wszystkie 3 zmienne wskazują na pierwsze konto, a o drugim "zapominamy"
print('a:', a)
print('b:', b)
print('c:', c)
print()

# W Pythonie (inaczej niż w C, a podobnie jak w Javie) nie usuwa się obiektów z pamięci
# - zajmuje się tym interpreter, a dokładnie "garbage collector".

# Jeśli chcemy jawnie wpisać, że zmienna nie wskazuje na żaden obiekt, to można tak:
a = None
print('a:', a)
print('b:', b)
print('c:', c)
print()

# w Pythonie można jawnie usunąć zmienną globalną,
# ale to nie oznacza usunięcia obiektu (inaczej niż delete w C++)
del b

# od tej pory nie mogę używać tej nazwy zmiennej,
# ale jeśli istnieje inna zmienna wskazująca na obiekt, to obiekt wciąż żyje i można go używać
print('a:', a)
# print('b:', b) # NameError: name 'b' is not defined
print('c:', c)
