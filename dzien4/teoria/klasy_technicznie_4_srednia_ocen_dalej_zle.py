# Również domyślne wartości metod (i funkcji) mogą być zdradliwe...

class Osoba:
    imie = 'imie'
    nazwisko = 'nazwisko'

    def __str__(self):
        return f'{self.imie} {self.nazwisko}'


class Student(Osoba):
    # Myślimy tak: można podać oceny studenta, a jeśli nie podamy, to wtedy jest przyjęta pusta lista
    # Niespodzianka: Wszyscy studenci tworzeni bez parametru oceny będą używać TEJ SAMEJ WSPOLNEJ LISTY
    def __init__(self, oceny=[]):
        self.oceny = oceny

    def __str__(self):
        return f'{super().__str__()} , oceny: {self.oceny}'

    def dodaj_ocene(self, ocena):
        self.oceny.append(ocena)

    def srednia(self):
        return sum(self.oceny) / len(self.oceny)


a = Student()
print(a)
a.imie = 'Adam'
a.nazwisko = 'Abacki'

b = Student()
b.imie = 'Bartek'
b.nazwisko = 'Babecki'

c = Student([3,4,5])
c.imie = 'Cezary'
c.nazwisko = 'Czajkowski'

print(a)
print(b)
print(c)
print()
# Do tej pory to działało dobrze

print('Dodajemy oceny studentowi a...')
a.dodaj_ocene(5)
a.dodaj_ocene(5)
a.dodaj_ocene(5)
print(a)
print(b)
print(c)
print()

print('Dodajemy ocenę studentowi b...')
b.dodaj_ocene(3)
print(a)
print(b)
print(c)
print()

print('Średnie:')
print(a.imie, a.srednia())
print(b.imie, b.srednia())
print(c.imie, c.srednia())
