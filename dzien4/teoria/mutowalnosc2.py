
def dodaj1(lista, *nowe_elementy):
    lista.extend(nowe_elementy)
    print('dodaj1 na końcu:', lista)


def dodaj2(lista, *nowe_elementy):
    # przy takim zapisie nowy obiekt jest zapisywany na zmienną lista
    lista = lista + list(nowe_elementy)
    # lista += list(nowe_elementy) - to by zmieniło oryginalną listę
    print('dodaj2 na końcu:', lista)


lista = ['AAA', 'BBB']
print('początek:', lista)
print()

lista = lista + ['CCC', 'DDD']
print('po lista = lista+...:', lista)
print()

dodaj1(lista, 'EEE', 'FFF')
print('po dodaj1:', lista)
print()

dodaj2(lista, 'GGG', 'HHH')
print('po dodaj2:', lista)
