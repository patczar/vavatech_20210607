class Osoba:
    def __init__(self, imie, nazwisko, wiek):
        self.imie = imie
        self.nazwisko = nazwisko
        self.wiek = wiek

    def przedstawSie(self):
        print(f'Nazywam się {self.imie} {self.nazwisko} i mam {self.wiek} lat')

    def jestPelnoletnia(self):
        return self.wiek >= 18

    def __str__(self):
        # str zwraca napis przeznaczony "dla ludzi", obiekt przedstawiony tekstowo
        return f'{self.imie} {self.nazwisko} ({self.wiek} lat)'

    def __repr__(self):
        # repr zwraca napis przeznaczony "dla Pythona", tekstowa serializacja
        # o ile to możliwe, wrzucając wynikowy napis do interpretera Pythona powinniśmy uzyskać obiekt taki sam jak bieżący
        return f'Osoba({repr(self.imie)}, {repr(self.nazwisko)}, {repr(self.wiek)})'


a = Osoba('Ala', 'Kowalska', 30)
b = Osoba('Ola', 'Malinowska', 14)
print(a, b)
print('str(a):', str(a))
print('repr(a):', repr(a))
