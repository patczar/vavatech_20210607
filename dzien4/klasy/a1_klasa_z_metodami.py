class Osoba:
    def przedstawSie(self):
        print('Witaj, jestem jakąś osobą.')

    def jestPelnoletnia(self):
        return True


a = Osoba()
print(a)
a.przedstawSie()
if a.jestPelnoletnia():
    print('może kupić piwo')
else:
    print('nie może kupić piwa')
