class Osoba:
    pass


a = Osoba()
b = Osoba()
print(a)
print(b)

print(a == b)
print('hash:', hash(a), hash(b))
print(type(a))
print(type(a) == type(b))

# W Pythonie działa garbage collector i obiektów jawnie się nie usuwa.
# Wedle powszchnej opinii GC w Pythonie działa znacznie gorzej od tego z Javy (i zapewne .NET).

# Uwaga: operacja del nie usuwa obiektu, tylko usuwa zmienną. Natomiast obiekt może być usunięty z opóźnieniem
# i tylko wtedy gdy nie wskazuje na niego żadna inna zmienna

x = a
print('a:', a)
print('x:', x)

del a
# print('a:', a) # błąd - zmienna a już nie istneje, nie mogę używać tej nazwy
print('x:', x) # OK, obiekt nadal istnieje

del x # usuwam ostatnie dowiązanei do obiektu - teraz GC ma prawo usunąć obiekt
