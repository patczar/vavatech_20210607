# Tak zazwyczaj definiuje się klasy w Pythonie
class Osoba:
    def __init__(self, imie, nazwisko, wiek):
        self.imie = imie
        self.nazwisko = nazwisko
        self.wiek = wiek

    def przedstawSie(self):
        print(f'Nazywam się {self.imie} {self.nazwisko} i mam {self.wiek} lat')

    def jestPelnoletnia(self):
        return self.wiek >= 18



a = Osoba('Ala', 'Kowalska', 30)
b = Osoba('Ola', 'Malinowska', 14)

# Teraz te atrybuty można odczytywać, modyfikować...
print('Osoba ma na imię', a.imie)
a.wiek += 1
print()

# A jeśli metoda korzysta z tych atrybutów, to będzie miałą do nich dostęp poprzez parametr self
a.przedstawSie()
b.przedstawSie()

for x in [a, b]:
    if x.jestPelnoletnia():
        print(x.imie, 'może kupić piwo')
    else:
        print(x.imie, 'nie może kupić piwa')


# Teraz już nie da się utworzyć obiektu bez podania wymaganych parametrów
# c = Osoba()
