class Osoba:
    def __init__(self, imie, nazwisko, wiek):
        self.imie = imie
        self.nazwisko = nazwisko
        self.wiek = wiek

    def przedstawSie(self):
        print(f'Nazywam się {self.imie} {self.nazwisko} i mam {self.wiek} lat')

    def jestPelnoletnia(self):
        return self.wiek >= 18

    def __str__(self):
        return f'{self.imie} {self.nazwisko} ({self.wiek} lat)'

    def __repr__(self):
        return f'Osoba({repr(self.imie)}, {repr(self.nazwisko)}, {repr(self.wiek)})'


# Prosty przykład podklasy, która dodaje tylko nową metodę, ale nie zmienia atrybutów.
class Student(Osoba):
    def sredniaOcen(self):
        return 4.5


# Zauważmy, że podklasa dziedziczy metodę init, czyli obiekty Student trzeba tworzyć podając pola imie, nazwisko, wiek
student = Student('A', 'B', 20)
print(student)

# Na obiekcie podklasy można wywołać zaróœno metody odziedziczone z nadklasy, jak i nowe metody dodane w podklasie
student.przedstawSie()
print(student.sredniaOcen())
