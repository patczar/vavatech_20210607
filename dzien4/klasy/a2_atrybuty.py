# To nie jest przykada do naśladowania, on tylko pokazuje czym atrybuty są technicznie.

class Osoba:
    def przedstawSie(self):
        print(f'Nazywam się {self.imie} {self.nazwisko} i mam {self.wiek} lat')

    def jestPelnoletnia(self):
        return self.wiek >= 18



a = Osoba()
b = Osoba()
print(a, b)

# Do obiektu można w każdej chwili dodać atrybut (pole), czyli zmienną pamiętaną w obiekcie:
a.imie = 'Ala'
a.nazwisko = 'Kowalska'
a.wiek = 30

b.imie = 'Ola'
b.nazwisko = 'Malinowska'
b.wiek = 14

# Teraz te atrybuty można odczytywać, modyfikować...
print('Osoba ma na imię', a.imie)
a.wiek += 1
print()

# A jeśli metoda korzysta z tych atrybutów, to będzie miałą do nich dostęp poprzez parametr self
a.przedstawSie()
b.przedstawSie()

for x in [a, b]:
    if x.jestPelnoletnia():
        print(x.imie, 'może kupić piwo')
    else:
        print(x.imie, 'nie może kupić piwa')

# Dodawanie atrybutów w kodzie poza klasą jest ryzykowne i byłoby złą praktyką.
# Bo wtedy łatwo byłoby "zapomnieć" o dodaniu atrybutu i wtedy metoda nie zadziałą - brak atrybutu

c = Osoba()
c.imie = 'Jan'
# "zapominam" ustawić wiek i nazwisko
# if c.jestPelnoletnia():  # AttributeError: 'Osoba' object has no attribute 'wiek'
#     c.przedstawSie()
# else:
#     print('nie ma piwa')
