from employee_v2 import Employee

lista = Employee.wczytaj('pracownicy.csv')

# W tym pliku mamy klasyczny algorytm grupowania za pomocą słowników.
# Nie używamyh żadnych skrótów i sztuczek.

# W tym słowniku dla nazwy stanowiska będzie pamiętana suma pensji pracowników z tego stanowiska
sumy = {}
ilosci = {}

for emp in lista:
    if emp.job_title in sumy:
        # jeśli to jest kolejny pracownik z danego stanowiska
        sumy[emp.job_title] += emp.salary
        ilosci[emp.job_title] += 1
    else:
        # jeśli to był pierwszy pracownik z tego stanowiska
        sumy[emp.job_title] = emp.salary
        ilosci[emp.job_title] = 1

for job in sumy.keys():
    srednia = sumy[job] / ilosci[job]
    print(f'{job:32} {ilosci[job]:2} {srednia:10.2f}')
