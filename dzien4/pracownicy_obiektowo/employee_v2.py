from decimal import Decimal

class Employee:
    SEP = ';'

    def __init__(self, id, first_name, last_name, job_title, salary, hire_date, department_name, street_address, postal_code, city, country):
        self.id = int(id)
        self.first_name = first_name
        self.last_name = last_name
        self.job_title = job_title
        self.salary = Decimal(salary)
        self.hire_date = hire_date
        self.department_name = department_name
        self.street_address = street_address
        self.postal_code = postal_code
        self.city = city
        self.country = country

    def __str__(self):
        return f'Employee {self.id}: {self.first_name} {self.last_name} ({self.job_title}) {self.salary} USD'

    @property
    def csv_fields(self):
        return (self.id, self.first_name, self.last_name, self.job_title, self.salary, self.hire_date,
                self.department_name, self.street_address, self.postal_code, self.city, self.country)

    # Metoda statyczna to taka "funkcja", która należy do klasy (bo jej działanie jest ściśle związane w tą klasą)
    # ale jednocześnie nie jest metodą obiektu, nie wywołuje się jej na pojedynczym obiekcie.
    # Bardzo często są to metody służące do tworzenai obiektów albo wczytywania danych.
    @staticmethod
    def wczytaj(sciezka):
        lista = []
        with open(sciezka, mode='r', encoding='utf-8') as plik:
            for linia in plik:
                pola = linia.strip().split(Employee.SEP)
                emp = Employee(*pola)
                lista.append(emp)
        return lista

    @staticmethod
    def zapisz(employees, sciezka):
        with open(sciezka, mode='w', encoding='utf-8') as wyjscie:
            for emp in employees:
                print(*emp.csv_fields, sep=Employee.SEP, file=wyjscie)

    @staticmethod
    def zapisz_wersja_dict(employees, sciezka):
        with open(sciezka, mode='w', encoding='utf-8') as wyjscie:
            for emp in employees:
                print(*emp.__dict__.values(), sep=Employee.SEP, file=wyjscie)
                # w tej wersji do pliku zapisujemy wartości wszystkich pól obiektu - nie mając nad tym specjalnej kontroli
