from employee_v2 import Employee

def srednia_dla_miasta(lista, miasto):
    suma = 0
    ilosc = 0
    for emp in lista:
        if emp.city == miasto:
            suma += emp.salary
            ilosc += 1
    if ilosc == 0:
        return 0
    else:
        return suma / ilosc


def main():
    lista = Employee.wczytaj('pracownicy.csv')

    while True:
        miasto = input('Podaj miasto: ')
        if not miasto: break
        srednia = srednia_dla_miasta(lista, miasto)
        print(f'W mieście {miasto} zarabia się średnio {srednia:.1f} USD')


if __name__ == '__main__':
    main()
