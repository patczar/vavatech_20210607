from employee_v1 import *

lista = wczytaj('pracownicy.csv')
print(f'Wczytano {len(lista)} rekordów.\n')
for emp in lista:
    print(f'|{emp.first_name:20}|{emp.last_name:20}|{emp.salary:8}|{emp.country:30}|')

print('\nZapiszę plik kopia1')
zapisz(lista, 'kopia1.csv')
print('Gotowe')
