from employee_v2 import Employee

lista = Employee.wczytaj('pracownicy.csv')

bogaty = None
biedny = None

for emp in lista:
    if bogaty is None or emp.salary > bogaty.salary:
        bogaty = emp
    if biedny is None or emp.salary < biedny.salary:
        biedny = emp

print('Najbogatszy:', bogaty)
print('Najbiedniejszy:', biedny)
