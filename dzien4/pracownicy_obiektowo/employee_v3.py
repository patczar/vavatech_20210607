from collections import namedtuple

# Mniej kodowania dzięki konstrukcji namedtuple - to jest taka namiastka klasy,
# gdy tylko przechowuje się atrybuty, a nie ma metod.

Employee = namedtuple('Employee', ['id', 'first_name', 'last_name', 'job_title', 'salary',
                 'hire_date', 'department_name', 'street_address', 'postal_code', 'city', 'country'])

def wczytaj(sciezka):
    lista = []
    with open(sciezka, mode='r', encoding='utf-8') as plik:
        for linia in plik:
            pola = linia.strip().split(';')
            emp = Employee(int(pola[0]), pola[1], pola[2], pola[3], int(pola[4]),
                           pola[5], pola[6], pola[7], pola[8], pola[9], pola[10])
            lista.append(emp)
    return lista


def zapisz(employees, sciezka):
    with open(sciezka, mode='w', encoding='utf-8') as wyjscie:
        for emp in employees:
            print(*emp, sep=';', file=wyjscie)
