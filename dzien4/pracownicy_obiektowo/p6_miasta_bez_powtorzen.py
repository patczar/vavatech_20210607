from employee_v2 import Employee

lista = Employee.wczytaj('pracownicy.csv')

# wersja "imperatywna"
miasta = set()
for emp in lista:
    miasta.add(emp.city)
print(miasta)

# wersja "deklaratywna" z wykorzystaniem comprehention
miasta = {emp.city for emp in lista}
print(miasta)

miasta = {emp.city for emp in lista if emp.city}
print(miasta)
