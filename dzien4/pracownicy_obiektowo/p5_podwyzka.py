from decimal import Decimal
from employee_v2 import Employee

komu = input('Podaj stanowisko, np. Programmer: ')
podwyzka = Decimal(input('Podaj kwotę podwyżki: '))

lista = Employee.wczytaj('pracownicy.csv')
for emp in lista:
    if emp.job_title == komu:
        emp.salary += podwyzka

print('\nZapiszę plik po_podwyzce.csv')
Employee.zapisz(lista, 'po_podwyzce.csv')
print('Gotowe')
