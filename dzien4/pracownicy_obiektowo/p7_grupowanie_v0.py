from employee_v2 import Employee

lista = Employee.wczytaj('pracownicy.csv')

jobs = set()
for emp in lista:
    jobs.add(emp.job_title)

print(f'Różnych stanowisk jest {len(jobs)}')
print(jobs)
print()

for job in jobs:
    suma = ilosc = 0
    for emp in lista:
        if emp.job_title == job:
            suma += emp.salary
            ilosc += 1
    srednia = suma / ilosc
    print(f'{job:32} {ilosc:2} {srednia:10.2f}')
