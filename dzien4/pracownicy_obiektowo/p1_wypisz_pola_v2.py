from employee_v2 import Employee

lista = Employee.wczytaj('pracownicy.csv')
print(f'Wczytano {len(lista)} rekordów.\n')
for emp in lista:
    print(f'|{emp.first_name:20}|{emp.last_name:20}|{emp.salary:8}|{emp.country:30}|')

print('\nZapiszę plik kopia2')
Employee.zapisz(lista, 'kopia2.csv')
print('Gotowe')
