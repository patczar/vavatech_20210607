from collections import namedtuple

with open('pracownicy_z_naglowkiem.csv', mode='r', encoding='utf-8') as plik:
    linia_naglowkowa = plik.readline().strip()
    naglowki = linia_naglowkowa.split(';')
    Rekord = namedtuple('Rekord', naglowki)
    obiekty = [Rekord(*linia.strip().split(';')) for linia in plik]
    print('Ilość rekordów:', len(obiekty))
    for obiekt in obiekty:
        print(obiekt)
