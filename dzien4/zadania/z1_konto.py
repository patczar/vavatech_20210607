# Utwórz klasę Konto. Obiekt tej klasy przechowuje 3 informacje:
# * numer
# * właściciel
# * saldo
# Podczas tworzenia Konta można podać lub pominąć saldo - wtedy zostanie przyjęte 0.
# Zdefiniuj metody __str__ oraz __repr__
# Zdefiniuj metody wplata oraz wyplata, które pozwalają zwiększyć lub zmniejszyć saldo konta o podaną kwotę.

class Konto:
    def __init__(self, numer, wlasciciel, saldo=0):
        self.numer = numer
        self.wlasciciel = wlasciciel
        self.saldo = saldo

    def wplata(self, kwota):
        self.saldo += kwota

    def wyplata(self, kwota):
        self.saldo -= kwota

    def __str__(self):
        return f'Konto nr {self.numer}, wł. {self.wlasciciel}, saldo: {self.saldo} PLN'

    def __repr__(self):
        return f'Konto({repr(self.numer)}, {repr(self.wlasciciel)}, {repr(self.saldo)})'


a = Konto(1, 'Ala', 1000)
b = Konto(2, 'Ola')
print('a:', a)
print('b:', b)

a.wyplata(300)
print('a:', a)

b.wplata(200)
print('b:', b)
